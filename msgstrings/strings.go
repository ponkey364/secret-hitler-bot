package msgstrings

const (
	INVITE_MSG   = "Hi %s, %s invited you to play Secret Hitler.\nWould you like to play?\n*type `?ignore` or `?join`*"
	REM_TO_START = "%d invited players have joined. Waiting on %d more. (the host can start the game with `?begin` " +
		", if there's enough players currently)"
)

// Elections and appointing
const (
	APPOINT_CHANCELLOR = "Please appoint a Chancellor. Type `?appoint {number}` and then one of the following options:\n"
	ELECTION_SUCCEED   = "The election has succeeded. <@!%s> is The President and <@!%s> is The Chancellor"
	PLEASE_VOTE        = "The president wants <@!%s> to be the Chancellor, vote `?vote ja` or `?vote nein`"
	APPOINT_OPT        = "%d. Type `?appoint %d` for <@!%s>"
	STAT_ELCOUNT       = "The Election Counter is currently at %d out of 3"
)

// Policy
const (
	DISCARD_POLICY  = "The policies are %s, %s and %s.\nDiscard one by typing `?discard {number 1/2/3}`"
	CHAOS_POLICY    = "Amidst the chaos, a %s policy was enacted!"
	CHAN_SELECT_POL = "The policies passed to you are %s and %s. Please select one to enact with `?enact {number 1/2}`"
	ENACTED         = "The President and Chancellor have enacted a %s policy."
)

// Board & Powers
const (
	LOYAL_WHO           = "You can choose to reveal one player's party membership card using command `?p-loyalty {num}`.\n%s"
	LOYAL_REVEAL        = "Player <@!%s> is a member of the %s party.\nYou may choose to share this information or keep it to yourself"
	LOYAL_INVESTIGATION = "President <@!%s> has chosen to investigate your loyalty and discovered you are in the %s party"

	EXAMINE_DECK = "__Power: Policy Peek__\nThe top three cards on the deck are %s, %s and %s."

	SPECIAL_ELECTION_WHO = "__Power: Special Election__\nYou must choose the next president with command `?p-election {num}`\n%s"

	EXECUTE_WHO = "__Power: 🔫Execute🔫__\nYou must choose a person to execute with command `?p-execute {num}`\n%s"
	EXECUTED    = "💥💥**BANG**💥💥\nPresident <@!%s> has executed <@!%s>"
)

// Errors
const (
	TOO_MANY   = "Too many people (including yourself), inviting the first 9 (you can end the game with !endhitler"
	NOT_ENOUGH = "Not enough people to begin a game - there should be a total of 5, including yourself"
	HAS_BOT    = "You've attempted to invite a bot, this game can never begin. Please start a new game without a bot"
)

// Win conditions
const (
	WIN_ELECTED_HITLER = "**Game Over!**\nYou elected Hitler!"
	WIN_5_LIB_POL      = "**Game Over!**\n5 Liberal policies were enacted and the Liberals won!"
	WIN_6_FASC_POL     = "**Game Over!**\n6 Fascist policies were enacted and the Liberals won!"
	WIN_HITLER_KILLED  = "**Game Over!**\nHitler has been killed and the Liberals won!"
)

const CREDITS = "```Programmed and maintained by @ponkey364, based on the game by Goat, Wolf, & Cabbage.\n" +
	"The code is licensed under CC BY-NC-SA 4.0 and can be found at https://gitlab.com/ponkey364/secret-hitler-bot\n" +
	"This bot is running from %s\\#%s```"
