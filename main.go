package main

import (
	"context"
	"flag"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	cc "gitlab.com/RPGPN/criuscommander"
	"os"
	"os/signal"
	"secrethitlerbot/game"
	"secrethitlerbot/redi"
	"syscall"
)

var (
	CommitHash      string
	BuildTime       string
	HelpEmbedColour = 16711680
	BotURL          = "https://gitlab.com/ponkey364/secret-hitler-bot"
	HelpFooter      = "Secret Hitler Bot by ponkey364 (Built at " + BuildTime + " from #" + CommitHash[:6] + ")"
)

func main() {
	logLevel := flag.Int("log", 4, "logrus debugging level; 0=panic;1=fatal;2=error;3=warn;4=info (default);5=debug;6=trace")
	logCaller := flag.Bool("log-caller", false, "logrus report caller")

	flag.Parse()

	logrus.SetLevel(logrus.Level(*logLevel))
	logrus.SetReportCaller(*logCaller)

	// first, we need to connect to the db (redis)
	rdb := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_ADDR"),
		Password: os.Getenv("REDIS_PWD"),
		DB:       0,
	})

	_, err := rdb.Ping(context.TODO()).Result()
	if err != nil {
		panic(err)
	}

	cmder, err := cc.NewCommander(&cc.CCConfig{
		Prefix:              "?",
		Token:               os.Getenv("TOKEN"),
		HelpEmbedColour:     &HelpEmbedColour,
		URL:                 &BotURL,
		HelpEmbedFooterText: &HelpFooter,
	})

	if err != nil {
		panic(err)
	}

	game.PullVars(CommitHash, BuildTime)

	cmder.SetupPlugin(redi.InitSetup(rdb), ".redi")
	cmder.SetupPlugin(game.Setup, "game")

	cmder.Open()

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}
