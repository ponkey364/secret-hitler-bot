package enums

import "secrethitlerbot/types"

const (
	DECK_FASCIST types.Deck = 'F'
	DECK_LIBERAL types.Deck = 'L'
)

const (
	ROLE_HITLER  types.Role = "HITLER"
	ROLE_FASCIST types.Role = "FASCIST"
	ROLE_LIBERAL types.Role = "LIBERAL"
)

const (
	WIN_ELECT_HITLER types.WinCondition = iota
	WIN_6_FAC_POL
	WIN_5_LIB_POL
	WIN_KILL_HITLER
)

const (
	STAGE_APPOINT   types.Stage = "APPOINT"
	STAGE_VOTING    types.Stage = "VOTING"
	STAGE_DISCARD   types.Stage = "DISCARD"
	STAGE_ENACT     types.Stage = "ENACT"
	STAGE_POW_LOY   types.Stage = "POW_LOYALTY"
	STAGE_POW_EXEC  types.Stage = "POW_EXECUTE"
	STAGE_POW_SPEEL types.Stage = "POW_SPECELECT"
	STAGE_HOLD      types.Stage = "HOLD"  // nothing will work in hold
	STAGE_SETUP     types.Stage = "SETUP" // only join, ignore and the force start
)
