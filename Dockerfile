FROM golang:1.16-alpine as builder

LABEL maintainer="ponkey364"

RUN mkdir /bot
COPY . /bot
WORKDIR /bot

RUN apk update && apk add --no-cache git

RUN chmod +x ./build.sh
RUN ./build.sh

FROM alpine:latest
RUN apk --no-cache add ca-certificates

COPY --from=builder /bot/bot .

CMD [ "./bot" ]