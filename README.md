# Secret Hitler Bot
A discord bot for playing secret hitler (http://secrethitler.com/)

## Usage

### Playing
1. Invite the bot to your server with [this link](https://discord.com/oauth2/authorize/?permissions=67628096&scope=bot&client_id=406808100667129857)
2. Find 4-9 other people who want to play
3. Start a game with `?startgame` and tag everyone who you want to play with, like `?startgame @friend1 ... @friend5`
4. Enjoy

### Hosting
all you need is a discord bot token and a redis server.  
Docker persistence is only required needed for redis - seriously, the machine you run this on could explode with no data
loss and all games would still work fine provided redis was on a different server.

Unfortunately, it's not possible for me to offer builds currently apart from those in Docker.

Personally, I recommend Docker, there's even an example compose file. However, it also runs on:

#### Windows
Good luck:
1. Install Go 1.13 or higher
2. Download the source
3. Extract the source, navigate to it and call `go build .`
4. *(optional)* Make a cup of tea, you'll need it
5. Install [redis 5 for windows](https://github.com/tporadowski/redis/releases/) 
6. Set an environment variable containing your token, and another containing the address to Redis if you're smart and
so you decided to run redis on something it was designed for
7. *(optional)* Take a moment because you just had to figure out how to do that in Windows (I certainly did)
8. Start the bot

No, there's no easier way to do this apart from running in Docker or on a remote server running a better OS

#### Linux & macOS
1. Install Go 1.13 or higher
2. Download the source
3. Extract the source, navigate to it and call `go build .`
4. Install redis
5. Run with `TOKEN=token ./secrethitlerbot`

## License
As per http://secrethitler.com/, this project is licensed under CC BY-NC-SA 4.0. To view a copy of this license, visit
https://creativecommons.org/licenses/by-nc-sa/4.0