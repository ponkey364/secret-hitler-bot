package game

import (
	"context"
	"fmt"
	"gitlab.com/RPGPN/criuscommander"
	"math/rand"
	"secrethitlerbot/enums"
	"secrethitlerbot/msgstrings"
	"secrethitlerbot/redi"
	"secrethitlerbot/types"
	"secrethitlerbot/utils"
	"strconv"
	"time"
)

func enactNewPolicy(gameID string, policy types.Deck, kv *redi.Redi) error {
	var entryName string
	switch policy {
	case enums.DECK_FASCIST:
		entryName = "fac_pol"
	case enums.DECK_LIBERAL:
		entryName = "lib_pol"
	}

	err := kv.DirectDB().HIncrBy(context.Background(), fmt.Sprintf("game:%s", gameID), entryName, 1).Err()
	if err != nil {
		return err
	}

	return nil
}

func storeDeck(deck [3]types.Deck, gameID string, kv *redi.Redi) error {
	var stoDeck uint32 = 0

	for _, val := range deck {
		switch val {
		case enums.DECK_FASCIST:
			{
				stoDeck = stoDeck << 1
				stoDeck = stoDeck | 0x01
			}
		case enums.DECK_LIBERAL:
			{
				stoDeck = stoDeck << 1
			}
		}
	}

	err := kv.SetKeyOnGame(gameID, "current_cards", stoDeck).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *Game) startPolicyProcess(gameID string) error {
	var cards [3]types.Deck

	// pick 3 cards from the top of the deck
	for i, _ := range cards {
		card, err := utils.GetTopFromDeck(g.kv, gameID)
		if err != nil {
			return err
		}

		cards[i] = *card
	}

	presPtr, err := g.kv.GetKeyOnGame(gameID, "pres_ptr").Int64()
	if err != nil {
		return err
	}

	presidentID, err := g.kv.GetPlayerIDByNum(gameID, presPtr)
	if err != nil {
		return err
	}

	presChannel, err := g.kv.GetPlayerChannel(*presidentID)
	if err != nil {
		return err
	}

	err = storeDeck(cards, gameID, g.kv)
	if err != nil {
		return err
	}

	// set the stage to accept a discard
	err = g.kv.SetKeyOnGame(gameID, "game_stage", enums.STAGE_DISCARD).Err()
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*presChannel, fmt.Sprintf(msgstrings.DISCARD_POLICY, cards[0], cards[1], cards[2]))

	// after this is done, we should see if the deck has less than three cards in it, if it does, regen the deck and
	// bump deck_rem back up
	return checkDeckNotEmpty(gameID, g.kv)
}

func checkDeckNotEmpty(gameID string, kv *redi.Redi) error {
	remaining, err := kv.GetKeyOnGame(gameID, "deck_rem").Uint64()
	if err != nil {
		return err
	}

	if remaining < 3 {
		seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))

		facPol, err := kv.GetKeyOnGame(gameID, "fac_pol").Int()
		if err != nil {
			return err
		}
		libPol, err := kv.GetKeyOnGame(gameID, "lib_pol").Int()
		if err != nil {
			return err
		}

		deck := generateDeck(seededRand, facPol, libPol)
		err = kv.SetKeyOnGame(gameID, "deck", deck).Err()
		if err != nil {
			return err
		}

		err = kv.SetKeyOnGame(gameID, "deck_rem", 17-facPol-libPol).Err()
		if err != nil {
			return err
		}
	}

	return nil
}

func (g *Game) discardPolicy(m *criuscommander.MessageContext, args []string) error {
	// what game are we in?
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	discard, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	// get the current deck
	currentDeck, err := g.kv.GetKeyOnGame(*gameID, "current_cards").Uint64()
	if err != nil {
		return err
	}

	var fullDeck [3]types.Deck
	for i := 0; i < 3; i++ {
		if (currentDeck & 1) == 1 {
			fullDeck[i] = enums.DECK_FASCIST
		} else {
			fullDeck[i] = enums.DECK_LIBERAL
		}

		// sliiiiide to the right
		currentDeck = currentDeck >> 1
	}

	// everything is backwards
	var sDeck [2]types.Deck
	switch discard {
	case 1:
		sDeck = [2]types.Deck{fullDeck[0], fullDeck[1]}
	case 2:
		sDeck = [2]types.Deck{fullDeck[0], fullDeck[2]}
	case 3:
		sDeck = [2]types.Deck{fullDeck[1], fullDeck[2]}
	}

	chancellorNum, err := g.kv.GetKeyOnGame(*gameID, "chan_ptr").Int64()
	if err != nil {
		return err
	}

	chancellorID, err := g.kv.GetPlayerIDByNum(*gameID, chancellorNum)
	if err != nil {
		return err
	}

	// set the stage
	err = g.kv.SetKeyOnGame(*gameID, "game_stage", enums.STAGE_ENACT).Err()
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*chancellorID, fmt.Sprintf(msgstrings.CHAN_SELECT_POL, sDeck[0], sDeck[1]))

	// we still technically store as 3 bits, we just ignore any bits not in the 1 or 2 place
	// it's much easier than making another function to handle it.
	err = storeDeck([3]types.Deck{enums.DECK_LIBERAL, sDeck[0], sDeck[1]}, *gameID, g.kv)
	if err != nil {
		return err
	}

	return nil
}

func (g *Game) enactPolicy(m *criuscommander.MessageContext, args []string) error {
	// what game are we in?
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	enact, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	// get the current deck
	currentDeck, err := g.kv.GetKeyOnGame(*gameID, "current_cards").Uint64()
	if err != nil {
		return err
	}

	var fullDeck [2]types.Deck
	for i := 0; i < 2; i++ {
		if (currentDeck & 1) == 1 {
			fullDeck[i] = enums.DECK_FASCIST
		} else {
			fullDeck[i] = enums.DECK_LIBERAL
		}

		// sliiiiide to the right
		currentDeck = currentDeck >> 1
	}

	var cardToEnact types.Deck
	switch enact {
	case 1:
		cardToEnact = fullDeck[1]
	case 2:
		cardToEnact = fullDeck[0]
	}

	err = enactNewPolicy(*gameID, cardToEnact, g.kv)
	if err != nil {
		return err
	}

	globalChannel, err := g.kv.GetGlobalChannel(*gameID)
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*globalChannel, fmt.Sprintf(msgstrings.ENACTED, cardToEnact))

	return g.postEnact(*gameID, cardToEnact, true)
}

func (g *Game) postEnact(gameID string, justEnacted types.Deck, canUsePowers bool) error {
	didWin, winReason, err := postEnactCheckWin(gameID, g.kv)
	if err != nil {
		return err
	}

	if didWin {
		return g.handleWin(gameID, *winReason)
	}

	// reset the election counter
	err = g.kv.SetKeyOnGame(gameID, "election_counter", 0).Err()
	if err != nil {
		return err
	}

	// before we do a powers check, have we just enacted an F pol?
	if justEnacted == enums.DECK_FASCIST && canUsePowers {
		// check for powers
		facPol, err := g.kv.GetKeyOnGame(gameID, "fac_pol").Int()
		if err != nil {
			return err
		}

		playerCount, err := g.kv.GetGamePlayerCount(gameID)
		if err != nil {
			return err
		}

		switch *playerCount {
		case 5, 6:
			return Board1[facPol](g, gameID)
		case 7, 8:
			return Board2[facPol](g, gameID)
		case 9, 10:
			return Board3[facPol](g, gameID)
		}
	} else {
		// and remember to start the next election for lib pols
		return g.startElection(gameID)
	}

	return nil
}
