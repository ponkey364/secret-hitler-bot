package game

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"gitlab.com/RPGPN/criuscommander"
	"secrethitlerbot/enums"
	"secrethitlerbot/msgstrings"
	"secrethitlerbot/types"
	"secrethitlerbot/utils"
	"strconv"
)

type Board []func(*Game, string) error

// The 0th item will always be _Blank, because we don't want the first thing to run without a policy being enacted

var Board1 = Board{
	_Blank,
	_Blank,
	_Blank,
	PolicyPeek,
	Kill,
	Kill,
}

var Board2 = Board{
	_Blank,
	_Blank,
	Loyalty,
	SpecialElection,
	Kill,
	Kill,
}

var Board3 = Board{
	_Blank,
	Loyalty,
	Loyalty,
	SpecialElection,
	Kill,
	Kill,
}

// we just need to call another election - there's no power here
func _Blank(g *Game, gameID string) error {
	return g.startElection(gameID)
}

func PolicyPeek(g *Game, gameID string) error {
	var readDeck [3]types.Deck

	// first, get the deck
	deck, err := g.kv.GetKeyOnGame(gameID, "deck").Uint64()
	if err != nil {
		return err
	}

	for i := 0; i < 3; i++ {
		// 1 = fascist, 0 = liberal
		if (deck & 1) == 1 {
			readDeck[i] = enums.DECK_FASCIST
		} else {
			readDeck[i] = enums.DECK_LIBERAL
		}

		// sliiiiide to the right
		deck = deck >> 1
	}

	presPtr, err := g.kv.GetKeyOnGame(gameID, "pres_ptr").Int64()
	if err != nil {
		return err
	}

	president, err := g.kv.GetPlayerIDByNum(gameID, presPtr)
	if err != nil {
		return err
	}

	presidentChannel, err := g.kv.GetPlayerChannel(*president)
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*presidentChannel, fmt.Sprintf(msgstrings.EXAMINE_DECK, readDeck[0], readDeck[1], readDeck[2]))

	return g.startElection(gameID)
}

func Loyalty(g *Game, gameID string) error {
	// who is our president, they can't investigate themselves
	presPtr, err := g.kv.GetKeyOnGame(gameID, "pres_ptr").Int64()
	if err != nil {
		return err
	}

	president, err := g.kv.GetPlayerIDByNum(gameID, presPtr)
	if err != nil {
		return err
	}

	// and grab their channel too
	presidentChannel, err := g.kv.GetPlayerChannel(*president)
	if err != nil {
		return err
	}

	allPlayers, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}

	// generate the message
	// at this point, nobody is dead, so we only have to discount the president and anyone who has already been
	// investigated
	msg := ""

	for i, player := range allPlayers {
		if player == *president {
			continue
		}

		var beenInvestigated bool
		err := g.kv.GetKeyOnPlayer(player, "investigated").Scan(&beenInvestigated)
		if err != nil {
			return err
		}

		if beenInvestigated {
			continue
		}
		msg += fmt.Sprintf("%d. <@!%s>", i, player)
	}

	// enable input for the loyalty check command
	err = g.kv.SetKeyOnGame(gameID, "game_stage", enums.STAGE_POW_LOY).Err()
	if err != nil {
		return err
	}

	// and send
	g.s.ChannelMessageSend(*presidentChannel, fmt.Sprintf(msgstrings.LOYAL_WHO, msg))

	return nil
}

func SpecialElection(g *Game, gameID string) error {
	presPtr, err := g.kv.GetKeyOnGame(gameID, "pres_ptr").Int64()
	if err != nil {
		return err
	}

	president, err := g.kv.GetPlayerIDByNum(gameID, presPtr)
	if err != nil {
		return err
	}

	// and grab their channel too
	presidentChannel, err := g.kv.GetPlayerChannel(*president)
	if err != nil {
		return err
	}

	eligible := []*types.PlayersNum{}

	allPlayers, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}

	killedPlayers, err := utils.GetKilledPlayers(g.kv, gameID)
	if err != nil {
		return err
	}

	for i, player := range allPlayers {
		// the only people who *aren't* eligible are the current president and those who are dead.
		if player == *president || utils.SliceIndex(killedPlayers, player) != -1 {
			continue
		}

		eligible = append(eligible, &types.PlayersNum{PlayerID: player, Num: uint(i)})
	}

	msg := ""
	for _, player := range eligible {
		if player.PlayerID == *president {
			continue
		}
		msg += fmt.Sprintf(msgstrings.APPOINT_OPT, player.Num, player.Num, player.PlayerID)
	}

	// enable input for the special election command
	err = g.kv.SetKeyOnGame(gameID, "game_stage", enums.STAGE_POW_SPEEL).Err()
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*presidentChannel, fmt.Sprintf(msgstrings.SPECIAL_ELECTION_WHO, msg))

	return nil
}

func Kill(g *Game, gameID string) error {
	// who is our president, they can't kill themself
	presPtr, err := g.kv.GetKeyOnGame(gameID, "pres_ptr").Int64()
	if err != nil {
		return err
	}

	president, err := g.kv.GetPlayerIDByNum(gameID, presPtr)
	if err != nil {
		return err
	}

	// and grab their channel too
	presidentChannel, err := g.kv.GetPlayerChannel(*president)
	if err != nil {
		return err
	}

	allPlayers, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}

	// who has already been killed (gives a num)
	killed1, err := g.kv.GetKeyOnGame(gameID, "killed_1").Int()
	if err != nil && err != redis.Nil {
		return err
	}

	if err == redis.Nil {
		// otherwise it'll get set to 0 :(
		killed1 = -1
	}

	// generate the message
	// we have to discount the president and anyone who has already been killed
	msg := ""

	for i, player := range allPlayers {
		if player == *president || i == killed1 {
			continue
		}

		msg += fmt.Sprintf("%d. <@!%s>", i, player)
	}

	// enable input for the execute command
	err = g.kv.SetKeyOnGame(gameID, "game_stage", enums.STAGE_POW_EXEC).Err()
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*presidentChannel, fmt.Sprintf(msgstrings.EXECUTE_WHO, msg))

	return nil
}

// Handlers

func (g *Game) handleMembershipCheck(m *criuscommander.MessageContext, args []string) error {
	investigate, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	// what game are we in?
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	err = g.kv.SetKeyOnGame(*gameID, "game_stage", enums.STAGE_HOLD).Err()
	if err != nil {
		return err
	}

	// who are we investigating and what's their channel?
	investigateID, err := g.kv.GetPlayerIDByNum(*gameID, int64(investigate))
	if err != nil {
		return err
	}

	investigatedChannel, err := g.kv.GetPlayerChannel(*investigateID)
	if err != nil {
		return err
	}

	// who is the president, and what is their channel?
	presPtr, err := g.kv.GetKeyOnGame(*gameID, "pres_ptr").Int64()
	if err != nil {
		return err
	}

	presidentID, err := g.kv.GetPlayerIDByNum(*gameID, presPtr)
	if err != nil {
		return err
	}

	presidentChannel, err := g.kv.GetPlayerChannel(*presidentID)
	if err != nil {
		return err
	}

	// get their role, mask it to be the party, then send it to the president
	var party types.Role
	role, err := g.kv.GetKeyOnPlayer(*investigateID, "role").Result()
	if err != nil {
		return err
	}
	switch types.Role(role) {
	case enums.ROLE_HITLER, enums.ROLE_FASCIST:
		party = enums.ROLE_FASCIST
	case enums.ROLE_LIBERAL:
		party = enums.ROLE_LIBERAL
	}

	g.s.ChannelMessageSend(*presidentChannel, fmt.Sprintf(msgstrings.LOYAL_REVEAL, *investigateID, party))
	g.s.ChannelMessageSend(*investigatedChannel, fmt.Sprintf(msgstrings.LOYAL_INVESTIGATION, *presidentID, party))

	// and finally, call the election
	return g.startElection(*gameID)
}

func (g *Game) handleSpecialElection(m *criuscommander.MessageContext, args []string) error {
	newPres, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	err = g.kv.SetKeyOnGame(*gameID, "game_stage", enums.STAGE_HOLD).Err()
	if err != nil {
		return err
	}

	// set the reset_ptr to the special president, then call startElection to go through the motions normally
	// incrPresPtr will handle getting the reset_ptr and copying it.
	err = g.kv.SetKeyOnGame(*gameID, "reset_ptr", newPres).Err()
	if err != nil {
		return err
	}

	// who is/was the president?
	presPtr, err := g.kv.GetKeyOnGame(*gameID, "pres_ptr").Int64()
	if err != nil {
		return err
	}

	err = g.startElection(*gameID)
	if err != nil {
		return err
	}

	// now let's move the president pointer along to the next slot, but not save it yet
	// get the player count
	playerCount, err := g.kv.GetGamePlayerCount(*gameID)
	if err != nil {
		return err
	}

	if presPtr == int64(*playerCount-1) {
		// set presPtr to 0, by effect pulling us back to the start
		presPtr = 0
	} else {
		presPtr += 1
	}

	// and now we can set reset_ptr to the actual next president should a special election not have happened
	err = g.kv.SetKeyOnGame(*gameID, "reset_ptr", presPtr).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *Game) handleExecution(m *criuscommander.MessageContext, args []string) error {
	execute, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	err = g.kv.SetKeyOnGame(*gameID, "game_stage", enums.STAGE_HOLD).Err()
	if err != nil {
		return err
	}

	globalChannel, err := g.kv.GetGlobalChannel(*gameID)
	if err != nil {
		return err
	}

	executedID, err := g.kv.GetPlayerIDByNum(*gameID, int64(execute))
	if err != nil {
		return err
	}

	presPtr, err := g.kv.GetKeyOnGame(*gameID, "pres_ptr").Int64()
	if err != nil {
		return err
	}

	presidentID, err := g.kv.GetPlayerIDByNum(*gameID, presPtr)
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*globalChannel, fmt.Sprintf(msgstrings.EXECUTED, *presidentID, *executedID))

	isSecondKill, err := g.kv.DirectDB().HExists(context.Background(), fmt.Sprintf("game:%s", *gameID), "killed_1").Result()
	if err != nil {
		return err
	}

	// Players who get killed are stored in 2 variables, as a game can only kill 0,1 or 2 people
	if isSecondKill {
		err = g.kv.SetKeyOnGame(*gameID, "killed_2", execute).Err()
	} else {
		err = g.kv.SetKeyOnGame(*gameID, "killed_1", execute).Err()
	}

	if err != nil {
		return err
	}

	// was the kill hitler?
	killedRole, err := g.kv.GetKeyOnPlayer(*executedID, "role").Result()
	if err != nil {
		return err
	}

	if killedRole == string(enums.ROLE_HITLER) {
		return g.handleWin(*gameID, enums.WIN_KILL_HITLER)
	}

	// and start the next election
	return g.startElection(*gameID)
}
