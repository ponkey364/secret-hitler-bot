package game

import (
	"context"
	"fmt"
	"github.com/bwmarrin/discordgo"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"math/rand"
	"secrethitlerbot/enums"
	"secrethitlerbot/msgstrings"
	"secrethitlerbot/types"
	"time"
)

func (g *Game) handleNewGame(m *discordgo.MessageCreate) error {
	// first, create an ID for this game
	thisGameID := uuid.NewV4().String()

	// make an entry in the games table for this game with the "global channel"
	// What is a "global channel"? well, in real life playing SH, you'd talk to each other (for bluffing, elections etc.)
	// or use vc or something. We might not have that liberty, so we'll use a text channel
	err := g.kv.DirectDB().HSet(context.Background(), fmt.Sprintf("game:%s", thisGameID),
		map[string]interface{}{
			"global_channel":   m.ChannelID,
			"game_owner":       m.Author.ID,
			"election_counter": 0,
			"lib_pol":          0,
			"fac_pol":          0,
			"pres_ptr":         0,
			"chan_ptr":         0,
			"current_cards":    0,
			"deck":             0,
			"deck_rem":         0,
			"game_stage":       enums.STAGE_SETUP,
		}).Err()
	if err != nil {
		return err
	}

	// get the game owner's channel
	channel, err := g.s.UserChannelCreate(m.Author.ID)
	if err != nil {
		return err
	}

	// write the initing player's data into the table
	err = g.kv.WritePlayerData(m.Author.ID, thisGameID, channel.ID)
	if err != nil {
		return err
	}

	// and have them join
	err = g.kv.SetKeyOnPlayer(m.Author.ID, "is_joined", true).Err()
	if err != nil {
		return err
	}

	// start inviting people to the game
	for _, user := range m.Mentions {
		// quickly check if the user is a bot and if so throw an error
		if user.Bot {
			g.s.ChannelMessageSend(m.ChannelID, msgstrings.HAS_BOT)
			return nil
		}

		// first we make a channel (read: private message) with them
		channel, err := g.s.UserChannelCreate(user.ID)
		if err != nil {
			return err
		}

		// then we send the invite
		_, err = g.s.ChannelMessageSend(channel.ID, fmt.Sprintf(msgstrings.INVITE_MSG, user.Username, m.Author.Username))
		if err != nil {
			return err
		}

		// and write their data into the table
		err = g.kv.WritePlayerData(user.ID, thisGameID, channel.ID)
		if err != nil {
			return err
		}
	}

	return nil
}

func (g *Game) checkForStart(gameID string) error {
	totalPlayers, err := g.kv.GetGamePlayerCount(gameID)
	if err != nil {
		return err
	}

	var joinedPlayers uint64

	playerIDs, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}

	for _, id := range playerIDs {
		var isJoined bool
		err := g.kv.GetKeyOnPlayer(id, "is_joined").Scan(&isJoined)
		if err != nil {
			return err
		}

		if isJoined {
			joinedPlayers++
		}
	}

	if joinedPlayers == *totalPlayers {
		// we need to start the game, but first, lock commands out
		err = g.kv.SetKeyOnGame(gameID, "game_stage", enums.STAGE_HOLD).Err()
		if err != nil {
			return err
		}

		return g.startGame(gameID)
	}

	globalChannel, err := g.kv.GetGlobalChannel(gameID)
	if err != nil {
		return err
	}

	if *totalPlayers-joinedPlayers != 0 {
		_, err = g.s.ChannelMessageSend(*globalChannel, fmt.Sprintf(msgstrings.REM_TO_START, joinedPlayers, *totalPlayers-joinedPlayers))
		if err != nil {
			return err
		}
	}

	return nil
}

func (g *Game) startGame(gameID string) error {
	seededRand := rand.New(rand.NewSource(time.Now().UnixNano()))

	// set is running and put a message in the global channel
	globalChannel, err := g.kv.GetGlobalChannel(gameID)
	if err != nil {
		return err
	}

	// we're going to mention all of the players in this message
	// so, first, get all the player's IDs
	playerIDs, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}

	mentionString := ""
	for _, player := range playerIDs {
		mentionString += fmt.Sprintf("<@!%s> ", player)
	}
	g.s.ChannelMessageSend(*globalChannel, mentionString+" Please stand by, I'm setting the game up")

	// first, choose the fascists, liberals and hitler and alert them to their roles
	// let's find out how many people are in this game
	playerCount, err := g.kv.GetGamePlayerCount(gameID)
	if err != nil {
		return err
	}

	// how many of each role do we need?
	liberals, fascists := teamCount(*playerCount)

	// so, how we decide: we copy the slice, then use a shuffle
	players := playerIDs

	seededRand.Shuffle(len(players), func(i, j int) {
		players[i], players[j] = players[j], players[i]
	})

	// the first person is Hitler, 1:fascists are fascists, the rest are liberals
	hitlerID := players[0]
	err = g.setRole(enums.ROLE_HITLER, hitlerID)
	if err != nil {
		return err
	}

	fascistIDs := players[1 : fascists+1]
	for _, id := range fascistIDs {
		err = g.setRole(enums.ROLE_FASCIST, id)
		if err != nil {
			return err
		}
	}

	liberalIDs := players[fascists+1:]
	// just check to be sure
	if uint8(len(liberalIDs)) != liberals {
		// oh?
		logrus.Warn("Wrong amount of liberals?")
		return nil
	}

	for _, id := range liberalIDs {
		err = g.setRole(enums.ROLE_LIBERAL, id)
		if err != nil {
			return err
		}
	}

	// now call the deck generator
	deck := GenerateNewDeck(seededRand)
	// and write the deck to db
	err = g.kv.SetKeyOnGame(gameID, "deck", deck).Err()
	if err != nil {
		return err
	}

	// now, we need to tell all the fascists who the other fascists are
	// and, if the game has 5/6 players, we need the fascists and hitler to know each other
	go g.notifyFascists(*playerCount, fascistIDs, hitlerID)

	// now consider the game running
	err = g.kv.SetKeyOnGame(gameID, "is_running", true).Err()
	if err != nil {
		return err
	}

	return g.startElection(gameID)
}

func (g *Game) notifyFascists(playerCount uint64, fascists []string, hitler string) error {
	// tell the fascists who the other fascists are and who hitler is, but don't tell hitler this time
	if playerCount > 6 {
		for _, player := range fascists {
			channelID, err := g.kv.GetPlayerChannel(player)
			if err != nil {
				return err
			}

			mentions := ""

			for _, fac := range fascists {
				if fac == player {
					continue
				}
				mentions += fmt.Sprintf("||<@!%s>|| ", fac)
			}

			g.s.ChannelMessageSend(*channelID, fmt.Sprintf("The other Fascists are %s and Hitler is ||<@!%s>||",
				mentions, hitler))
		}
		return nil
	}

	// if there's 5 or 6 players, there's one facist, and they know who Hitler is and (crucially) Hitler knows them.

	facChannelID, err := g.kv.GetPlayerChannel(fascists[0])
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*facChannelID, fmt.Sprintf("Hitler is ||<@!%s>", hitler))

	hitlerChannelID, err := g.kv.GetPlayerChannel(hitler)
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*hitlerChannelID, fmt.Sprintf("The Fascist is ||<@!%s>||", fascists[0]))

	return nil
}

func (g *Game) setRole(role types.Role, playerID string) error {
	err := g.kv.SetKeyOnPlayer(playerID, "role", string(role)).Err()
	if err != nil {
		return err
	}

	channelID, err := g.kv.GetPlayerChannel(playerID)
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*channelID, fmt.Sprintf("Hello, your secret role is ||%s||\n**Please stand by for the game to begin**",
		role))

	return nil
}

func GenerateNewDeck(seed *rand.Rand) uint32 {
	return generateDeck(seed, 0, 0)
}

func generateDeck(seed *rand.Rand, usedFac int, usedLib int) uint32 {
	strDeck := []types.Deck{}

	for i := 0; i < (6 - usedLib); i++ {
		strDeck = append(strDeck, enums.DECK_LIBERAL)
	}
	for i := 0; i < (11 - usedFac); i++ {
		strDeck = append(strDeck, enums.DECK_FASCIST)
	}

	seed.Shuffle(len(strDeck), func(i, j int) {
		strDeck[i], strDeck[j] = strDeck[j], strDeck[i]
	})

	var deck uint32 = 0
	for _, val := range strDeck {
		switch val {
		case enums.DECK_FASCIST:
			{
				deck = deck << 1
				deck = deck | 0x01
			}
		case enums.DECK_LIBERAL:
			{
				deck = deck << 1
			}
		}
	}

	return deck
}

func teamCount(playerCount uint64) (uint8, uint8) {
	switch playerCount {
	case 5:
		return 3, 1
	case 6:
		return 4, 1
	case 7:
		return 4, 2
	case 8:
		return 5, 2
	case 9:
		return 5, 3
	case 10:
		return 6, 3
	}

	return 0, 0
}
