package game

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"secrethitlerbot/enums"
	"secrethitlerbot/msgstrings"
	"secrethitlerbot/redi"
	"secrethitlerbot/types"
	"secrethitlerbot/utils"
)

// sends the messages out, marks us as in an election
func (g *Game) startElection(gameID string) error {
	// increment the president_ptr
	presPtr, err := incrPresPtr(gameID, g.kv)
	if err != nil {
		return err
	}

	// who is our president?
	president, err := g.kv.GetPlayerIDByNum(gameID, *presPtr)
	if err != nil {
		return err
	}

	// get the global channel
	globalChannel, err := g.kv.GetGlobalChannel(gameID)
	if err != nil {
		return err
	}

	// now tell everyone
	g.s.ChannelMessageSend(*globalChannel, fmt.Sprintf("The President is <@!%s>", *president))

	// and message the president to tell them to appoint a chancellor
	presidentChannel, err := g.kv.GetPlayerChannel(*president)
	if err != nil {
		return err
	}

	// get a list of everyone who's eligible
	eligible := []*types.PlayersNum{}

	allPlayers, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}

	killedPlayers, err := utils.GetKilledPlayers(g.kv, gameID)
	if err != nil {
		return err
	}

	for i, player := range allPlayers {
		var isEligible bool
		err := g.kv.GetKeyOnPlayer(player, "eligible").Scan(&isEligible)
		if err != nil {
			return err
		}

		if utils.SliceIndex(killedPlayers, player) != -1 {
			continue
		}

		if isEligible {
			eligible = append(eligible, &types.PlayersNum{PlayerID: player, Num: uint(i)})
		}
	}

	msg := msgstrings.APPOINT_CHANCELLOR
	for _, player := range eligible {
		if player.PlayerID == *president {
			continue
		}
		msg += fmt.Sprintf(msgstrings.APPOINT_OPT, player.Num, player.Num, player.PlayerID)
	}

	// set the stage
	err = g.kv.SetKeyOnGame(gameID, "game_stage", enums.STAGE_APPOINT).Err()
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*presidentChannel, msg)

	return nil
}

func incrPresPtr(gameID string, kv *redi.Redi) (*int64, error) {
	// this function has three fun situations - we could need a reset pointer, we could have to skip a dead player,
	// OR we could have a reset pointer and *then* have to skip a dead player.

	// so, let's get any dead players
	killedPlayers, err := utils.GetKilledPlayers(kv, gameID)
	if err != nil {
		return nil, err
	}

	// get the player count
	playerCount, err := kv.GetGamePlayerCount(gameID)
	if err != nil {
		return nil, err
	}

	// first, check for the presence of a reset_ptr, this is used for special elections
	resetPtr, err := kv.GetKeyOnGame(gameID, "reset_ptr").Int64()
	if err == redis.Nil {
		// increment normally, there's no reset_ptr
		presPtr, err := kv.GetKeyOnGame(gameID, "pres_ptr").Int64()
		if err != nil {
			return nil, err
		}

		keepIncr := true

		for {
			if !keepIncr {
				break
			}

			if presPtr == int64(*playerCount-1) {
				// set presPtr to 0, by effect pulling us back to the start
				presPtr = 0
			} else {
				presPtr += 1
			}

			// get the presPtr's id and check if they're dead
			presID, err := kv.GetPlayerIDByNum(gameID, presPtr)
			if err != nil {
				return nil, err
			}

			if utils.SliceIndex(killedPlayers, *presID) != -1 {
				// yay, increment again
				continue
			}

			keepIncr = false
		}

		err = kv.SetKeyOnGame(gameID, "pres_ptr", presPtr).Err()
		if err != nil {
			return nil, err
		}

		return &presPtr, nil
	}
	if err != nil && err != redis.Nil {
		return nil, err
	}

	// we have a reset_ptr

	// remove the reset_ptr, so it doesn't get accidentally used again.
	err = kv.DirectDB().HDel(context.Background(), fmt.Sprintf("game:%s", gameID), "reset_ptr").Err()
	if err != nil {
		return nil, err
	}

	// get the resetPtr's id and check if they're dead
	presID, err := kv.GetPlayerIDByNum(gameID, resetPtr)
	if err != nil {
		return nil, err
	}

	// if they're dead, increment / pull to 0 like we usually do.
	// this isn't in a loop because only one person can be dead at this point.
	if utils.SliceIndex(killedPlayers, *presID) != -1 {
		if resetPtr == int64(*playerCount-1) {
			// set presPtr to 0, by effect pulling us back to the start
			resetPtr = 0
		} else {
			resetPtr += 1
		}
	}

	err = kv.SetKeyOnGame(gameID, "pres_ptr", resetPtr).Err()
	if err != nil {
		return nil, err
	}

	return &resetPtr, nil
}

// gets called whenever someone submits their vote
func (g *Game) checkForElectionFinish(gameID string) error {
	playerIDs, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}

	var notVoted int

	for _, player := range playerIDs {
		_, err := g.kv.GetKeyOnPlayer(player, "vote").Result()
		if errors.Is(err, redis.Nil) {
			notVoted++
			continue
		}
		if err != nil {
			return err
		}
	}

	if notVoted > 0 {
		// we need to wait
		return nil
	}

	return g.handleEndElection(gameID)
}

func (g *Game) handleElectionFail(gameID string) error {
	// increment the election_counter
	err := g.kv.DirectDB().HIncrBy(context.Background(), fmt.Sprintf("game:%s", gameID), "election_counter", 1).Err()
	if err != nil {
		return err
	}

	electionCounter, err := g.kv.GetKeyOnGame(gameID, "election_counter").Int64()

	globalChannel, err := g.kv.GetGlobalChannel(gameID)
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*globalChannel, fmt.Sprintf(msgstrings.STAT_ELCOUNT, electionCounter))

	if electionCounter == 3 {
		top, err := utils.GetTopFromDeck(g.kv, gameID)
		if err != nil {
			return err
		}

		var topString string

		switch *top {
		case enums.DECK_LIBERAL:
			topString = "Liberal"
		case enums.DECK_FASCIST:
			topString = "Fascist"
		}

		err = enactNewPolicy(gameID, *top, g.kv)
		if err != nil {
			return err
		}

		g.s.ChannelMessageSend(*globalChannel, fmt.Sprintf(msgstrings.CHAOS_POLICY, topString))

		return g.postEnact(gameID, *top, false)
	} else {
		// otherwise
		return g.startElection(gameID)
	}
}

func (g *Game) handleElectionSuccess(gameID string) error {
	// tell everyone
	globalChannel, err := g.kv.GetGlobalChannel(gameID)
	if err != nil {
		return err
	}

	presPtr, err := g.kv.GetKeyOnGame(gameID, "pres_ptr").Int64()
	if err != nil {
		return err
	}

	chanPtr, err := g.kv.GetKeyOnGame(gameID, "chan_ptr").Int64()
	if err != nil {
		return err
	}

	presidentID, err := g.kv.GetPlayerIDByNum(gameID, presPtr)
	if err != nil {
		return err
	}

	chancellorID, err := g.kv.GetPlayerIDByNum(gameID, chanPtr)
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*globalChannel, fmt.Sprintf(msgstrings.ELECTION_SUCCEED, *presidentID, *chancellorID))

	// clear the previous ineligible and set the current presidentID & chancellorID as ineligible
	players, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}
	for _, player := range players {
		g.kv.SetKeyOnPlayer(player, "eligible", true)
	}

	// we're quickly going to check if the president should be eligible or not
	// the rules tell us they are provided there are 5 or less players remaining.
	deadPlayers, err := utils.GetKilledPlayers(g.kv, gameID)
	if err != nil {
		return err
	}

	if len(players)-len(deadPlayers) > 5 {
		err = g.kv.SetKeyOnPlayer(*presidentID, "eligible", false).Err()
		if err != nil {
			return err
		}
	}

	err = g.kv.SetKeyOnPlayer(*chancellorID, "eligible", false).Err()
	if err != nil {
		return err
	}

	// check if: the chancellorID is hitler AND at least 3 fascist policies have been enacted
	hitlerWin, err := checkHitlerChancellorWin(*chancellorID, gameID, g.kv)
	if err != nil {
		return err
	}
	if hitlerWin {
		// win the game
		g.handleWin(gameID, enums.WIN_ELECT_HITLER)
		return nil
	}

	// the game continues - start the process of enacting policy
	return g.startPolicyProcess(gameID)
}

// called from checkForElectionFinish when the election is done - marks people as ineligible, sees if Ja or Nein won
// and handles calling another (or popping one from the deck) and incrementing the counter if required.
func (g *Game) handleEndElection(gameID string) error {
	// woo, everyone voted
	// let's count and see who won

	// set the stage to hold to stop any commands from working
	err := g.kv.SetKeyOnGame(gameID, "game_stage", enums.STAGE_HOLD).Err()
	if err != nil {
		return err
	}

	// first get all players
	players, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}

	var (
		jaVotes   int
		neinVotes int
	)

	for _, id := range players {
		vote, err := g.kv.GetKeyOnPlayer(id, "vote").Result()
		if err != nil {
			return err
		}

		switch vote {
		case "ja":
			jaVotes++
		case "nein":
			neinVotes++
		default:
			logrus.Warn("wait, what?")
		}

		// and clear their vote while we're here
		err = g.kv.DirectDB().HDel(context.Background(), fmt.Sprintf("players:%s", id), "vote").Err()
		if err != nil {
			return err
		}
	}

	if neinVotes >= jaVotes {
		return g.handleElectionFail(gameID)
	}

	return g.handleElectionSuccess(gameID)
}
