package game

import (
	"context"
	"fmt"
	"secrethitlerbot/enums"
	"secrethitlerbot/msgstrings"
	"secrethitlerbot/redi"
	"secrethitlerbot/types"
)

func (g *Game) handleWin(gameID string, condition types.WinCondition) error {
	// get the globalChannel
	globalChannel, err := g.kv.GetGlobalChannel(gameID)
	if err != nil {
		return err
	}

	// announce the win
	var winText string
	switch condition {
	case enums.WIN_5_LIB_POL:
		winText = msgstrings.WIN_5_LIB_POL
	case enums.WIN_6_FAC_POL:
		winText = msgstrings.WIN_6_FASC_POL
	case enums.WIN_ELECT_HITLER:
		winText = msgstrings.WIN_ELECTED_HITLER
	case enums.WIN_KILL_HITLER:
		winText = msgstrings.WIN_HITLER_KILLED
	}

	g.s.ChannelMessageSend(*globalChannel, winText)

	players, err := g.kv.GetGamePlayerIDs(gameID)
	if err != nil {
		return err
	}

	for _, player := range players {
		err = g.kv.DirectDB().Del(context.Background(), fmt.Sprintf("players:%s", player)).Err()
		if err != nil {
			return err
		}

		err = g.kv.DirectDB().HDel(context.Background(), "players>game", player).Err()
		if err != nil {
			return err
		}
	}

	err = g.kv.DirectDB().Del(context.Background(), fmt.Sprintf("game:%s", gameID), fmt.Sprintf("%s#players", gameID)).Err()
	if err != nil {
		return err
	}

	// and throw the credits
	g.s.ChannelMessageSend(*globalChannel, fmt.Sprintf(msgstrings.CREDITS, BuildTime, CommitHash[:6]))

	return nil
}

func checkHitlerChancellorWin(playerID string, gameID string, kv *redi.Redi) (bool, error) {
	facPol, err := kv.GetKeyOnGame(gameID, "fac_pol").Int()
	if err != nil {
		return false, err
	}

	if facPol < 3 {
		return false, nil
	}

	chancellorRole, err := kv.GetKeyOnPlayer(playerID, "role").Result()
	if err != nil {
		return false, err
	}

	if chancellorRole == string(enums.ROLE_HITLER) {
		return true, nil
	}

	return false, nil
}

// returns: if the game was won; a win reason; an error
func postEnactCheckWin(gameID string, kv *redi.Redi) (bool, *types.WinCondition, error) {
	facPol, err := kv.GetKeyOnGame(gameID, "fac_pol").Int()
	if err != nil {
		return false, nil, err
	}

	libPol, err := kv.GetKeyOnGame(gameID, "lib_pol").Int()
	if err != nil {
		return false, nil, err
	}

	if facPol == 6 {
		win := enums.WIN_6_FAC_POL
		return true, &win, nil
	}

	if libPol == 5 {
		win := enums.WIN_5_LIB_POL
		return true, &win, nil
	}

	return false, nil, nil
}
