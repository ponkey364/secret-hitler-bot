package game

import (
	"context"
	"fmt"
	"github.com/bwmarrin/discordgo"
	cc "gitlab.com/RPGPN/criuscommander"
	"secrethitlerbot/enums"
	"secrethitlerbot/msgstrings"
	"secrethitlerbot/redi"
	"secrethitlerbot/types"
	"secrethitlerbot/utils"
	"strconv"
	"strings"
)

var (
	CommitHash string
	BuildTime  string
)

func PullVars(commitHash string, buildTime string) {
	CommitHash = commitHash
	BuildTime = buildTime
}

func Setup(register cc.RegisterCommand, getPlugin cc.GetPlugin, s *discordgo.Session) (cc.CommandGroup, string) {
	kv, err := getPlugin(".redi")
	if err != nil {
		panic(err)
	}

	game := &Game{
		s:  s,
		kv: kv.(*redi.Redi),
	}

	// set the bot's status
	s.UpdateStatus(0, "?newgame for Secret Hitler")

	register("newgame", game.NewGame, "Make a new game - tag between 4 and 9 other people who you'd like to play with you")
	register("join", game.Join, "Join a game")
	register("ignore", game.Ignore, "Refuse the game invite")
	register("appoint", game.Appoint, "Appoint a Chancellor for the vote (President only)")
	register("vote", game.Vote, "Vote `ja` (yes) or `nein` (no) for the president and chancellor")
	register("discard", game.Discard, "Discard a policy (President only)")
	register("enact", game.Enact, "Enact a policy (Chancellor only)")

	register("p-loyalty", game.PowerLoyalty, "Investigate which party a player is a part of (President only, Special Power)")
	register("p-execute", game.PowerExecute, "Kill a player (President only, Special Power)")
	register("p-election", game.PowerElection, "Choose a player to be the president in a Special Election (President only, Special Power)")

	return game, "The main game controller"
}

type Game struct {
	s  *discordgo.Session
	kv *redi.Redi
}

// **GAME START**

func (g *Game) NewGame(m *cc.MessageContext, _ []string) error {
	// we need to invite people.
	// we presume the caller is going to be in the game, so we need a min of 4 and max of 9
	if len(m.Mentions) < 4 {
		// not enough
		g.s.ChannelMessageSend(m.ChannelID, msgstrings.NOT_ENOUGH)
		return nil
	}
	if len(m.Mentions) > 9 {
		// too many, invite the first 9
		g.s.ChannelMessageSend(m.ChannelID, msgstrings.TOO_MANY)
		return nil
	}

	return g.handleNewGame(m.MessageCreate)
}

func (g *Game) Join(m *cc.MessageContext, _ []string) error {
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	stage, err := g.kv.GetKeyOnGame(*gameID, "game_stage").Result()
	if err != nil {
		return err
	}

	if types.Stage(stage) != enums.STAGE_VOTING {
		return nil
	}

	// first, check if the player has already joined
	var isJoined bool
	err = g.kv.GetKeyOnPlayer(m.Author.ID, "is_joined").Scan(&isJoined)
	if err != nil {
		return err
	}

	if isJoined {
		m.Send("You're already in the game")
		return nil
	}

	err = g.kv.SetKeyOnPlayer(m.Author.ID, "is_joined", true).Err()
	if err != nil {
		return err
	}

	m.Send("Okay, you joined the game")

	// check if all the players are in, and if they are, start the game
	return g.checkForStart(*gameID)
}

func (g *Game) Ignore(m *cc.MessageContext, _ []string) error {
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	stage, err := g.kv.GetKeyOnGame(*gameID, "game_stage").Result()
	if err != nil {
		return err
	}

	if types.Stage(stage) != enums.STAGE_SETUP {
		return nil
	}

	err = g.kv.DirectDB().LRem(context.Background(), fmt.Sprintf("%s#players", gameID), 0, m.Author.ID).Err()
	if err != nil {
		return err
	}

	err = g.kv.DirectDB().HDel(context.Background(), "players>game", m.Author.ID).Err()
	if err != nil {
		return err
	}

	err = g.kv.DirectDB().Del(context.Background(), fmt.Sprintf("players:%s", m.Author.ID)).Err()
	if err != nil {
		return err
	}

	return nil
}

// **ELECTION**

func (g *Game) Appoint(m *cc.MessageContext, args []string) error {
	// what game are we in?
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	isPres, err := utils.IsSenderPresident(*gameID, m.Author.ID, g.kv)
	if err != nil {
		return err
	}
	if !isPres {
		return nil
	}

	stage, err := g.kv.GetKeyOnGame(*gameID, "game_stage").Result()
	if err != nil {
		return err
	}

	if types.Stage(stage) != enums.STAGE_APPOINT {
		return nil
	}

	// set the stage to hold to ignore any input
	err = g.kv.SetKeyOnGame(*gameID, "game_stage", enums.STAGE_HOLD).Err()
	if err != nil {
		return err
	}

	chancellorNumber, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	// put the person's num into chancellor_ptr
	err = g.kv.SetKeyOnGame(*gameID, "chan_ptr", chancellorNumber).Err()
	if err != nil {
		return err
	}

	// who has been appointed?
	appointedID, err := g.kv.GetPlayerIDByNum(*gameID, int64(chancellorNumber))
	if err != nil {
		return err
	}

	globalChannel, err := g.kv.GetGlobalChannel(*gameID)
	if err != nil {
		return err
	}

	// set the stage
	err = g.kv.SetKeyOnGame(*gameID, "game_stage", enums.STAGE_VOTING).Err()
	if err != nil {
		return err
	}

	g.s.ChannelMessageSend(*globalChannel, fmt.Sprintf(msgstrings.PLEASE_VOTE, *appointedID))

	return nil
}

func (g *Game) Vote(m *cc.MessageContext, args []string) error {
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	stage, err := g.kv.GetKeyOnGame(*gameID, "game_stage").Result()
	if err != nil {
		return err
	}

	if types.Stage(stage) != enums.STAGE_VOTING {
		return nil
	}

	var opt string
	choice := strings.ToLower(args[0])

	if choice == "ja" {
		opt = "ja"
	} else if choice == "nein" {
		opt = "nein"
	} else {
		return nil
	}

	err = g.kv.SetKeyOnPlayer(m.Author.ID, "vote", opt).Err()
	if err != nil {
		return err
	}

	return g.checkForElectionFinish(*gameID)
}

// **POLICY**

func (g *Game) Discard(m *cc.MessageContext, args []string) error {
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	isPres, err := utils.IsSenderPresident(*gameID, m.Author.ID, g.kv)
	if err != nil {
		return err
	}
	if !isPres {
		return nil
	}

	stage, err := g.kv.GetKeyOnGame(*gameID, "game_stage").Result()
	if err != nil {
		return err
	}

	if types.Stage(stage) != enums.STAGE_DISCARD {
		return nil
	}

	// disable input again
	err = g.kv.SetKeyOnGame(*gameID, "game_stage", enums.STAGE_HOLD).Err()
	if err != nil {
		return err
	}

	return g.discardPolicy(m, args)
}

func (g *Game) Enact(m *cc.MessageContext, args []string) error {
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	isChancellor, err := utils.IsSenderChancellor(*gameID, m.Author.ID, g.kv)
	if err != nil {
		return err
	}
	if !isChancellor {
		return nil
	}

	stage, err := g.kv.GetKeyOnGame(*gameID, "game_stage").Result()
	if err != nil {
		return err
	}

	if types.Stage(stage) != enums.STAGE_ENACT {
		return nil
	}

	// disable input again
	err = g.kv.SetKeyOnGame(*gameID, "game_stage", enums.STAGE_HOLD).Err()
	if err != nil {
		return err
	}

	return g.enactPolicy(m, args)
}

// **POWERS**
// TODO: give these functions better names or implement sub-commands

func (g *Game) PowerLoyalty(m *cc.MessageContext, args []string) error {
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	isPres, err := utils.IsSenderPresident(*gameID, m.Author.ID, g.kv)
	if err != nil {
		return err
	}
	if !isPres {
		return nil
	}

	stage, err := g.kv.GetKeyOnGame(*gameID, "game_stage").Result()
	if err != nil {
		return err
	}

	if types.Stage(stage) != enums.STAGE_POW_LOY {
		return nil
	}

	return g.handleMembershipCheck(m, args)
}

func (g *Game) PowerExecute(m *cc.MessageContext, args []string) error {
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	isPres, err := utils.IsSenderPresident(*gameID, m.Author.ID, g.kv)
	if err != nil {
		return err
	}
	if !isPres {
		return nil
	}

	stage, err := g.kv.GetKeyOnGame(*gameID, "game_stage").Result()
	if err != nil {
		return err
	}

	if types.Stage(stage) != enums.STAGE_POW_EXEC {
		return nil
	}

	return g.handleExecution(m, args)
}

func (g *Game) PowerElection(m *cc.MessageContext, args []string) error {
	gameID, err := g.kv.GetGameIDUsingPlayer(m.Author.ID)
	if err != nil {
		return err
	}

	isPres, err := utils.IsSenderPresident(*gameID, m.Author.ID, g.kv)
	if err != nil {
		return err
	}
	if !isPres {
		return nil
	}

	stage, err := g.kv.GetKeyOnGame(*gameID, "game_stage").Result()
	if err != nil {
		return err
	}

	if types.Stage(stage) != enums.STAGE_POW_SPEEL {
		return nil
	}

	return g.handleSpecialElection(m, args)
}

// might be used in the future
func (g *Game) OnReaction(session *discordgo.Session, i interface{}) error {
	return nil
}

// interface stuff we won't use
func (g *Game) OnGuildJoin(_ *discordgo.Session, _ *discordgo.GuildCreate) error  { return nil }
func (g *Game) OnGuildLeave(_ *discordgo.Session, _ *discordgo.GuildDelete) error { return nil }
func (g *Game) OnBotClose() error                                                 { return nil }
