GIT_COMMIT=$(git rev-list -1 HEAD)
BUILD_TIME=$(date +"%Y%m%d.%H%M%S")

CGO_ENABLED=0 go build -ldflags "-X main.CommitHash=$GIT_COMMIT -X main.BuildTime=$BUILD_TIME" -o bot