package types

type Role string
type Deck rune
type WinCondition uint
type Stage string

func (d Deck) String() string {
	switch d {
	case 'F':
		return "Fascist"
	case 'L':
		return "Liberal"
	}

	return string(d)
}

type Game struct {
	GameID          string `db:"game_id"`
	GlobalChannel   string `db:"global_channel"`
	IsRunning       bool   `db:"is_running"`
	ElectionCounter uint   `db:"election_counter"`
	LibPolicies     uint   `db:"lib_policies"`
	FacPolicies     uint   `db:"fac_policies"`
	GameOwner       string `db:"game_owner"`
	Deck            int
	PresidentPtr    int  `db:"president_ptr"`
	ChancellorPtr   int  `db:"chancellor_ptr"`
	DeckRem         int  `db:"deck_rem"`
	CurrentCards    *int `db:"current_cards"`
}

type Player struct {
	PlayerID     string `db:"player_id"`
	GameID       string `db:"game_id"`
	Joined       bool
	Role         Role
	IsIneligible bool   `db:"isIneligible"`
	ChannelID    string `db:"channel_id"`
	Vote         *string
	Num          int
}

type PlayersNum struct {
	PlayerID string `db:"player_id"`
	Num      uint
}
