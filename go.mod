module secrethitlerbot

go 1.13

require (
	github.com/bwmarrin/discordgo v0.22.0
	github.com/go-redis/redis/v8 v8.0.0-beta.6
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/RPGPN/criuscommander v1.1.2
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	golang.org/x/sys v0.0.0-20200917073148-efd3b9a0ff20 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
