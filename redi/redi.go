// functions for interfacing with redis
package redi

import (
	"context"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/go-redis/redis/v8"
	cc "gitlab.com/RPGPN/criuscommander"
)

type Redi struct {
	kv *redis.Client
}

func InitSetup(r *redis.Client) func(cc.RegisterCommand, cc.GetPlugin, *discordgo.Session) (cc.CommandGroup, string) {
	return func(_ cc.RegisterCommand, _ cc.GetPlugin, _ *discordgo.Session) (cc.CommandGroup, string) {
		return &Redi{kv: r}, "Redis Controller"
	}
}

func (r *Redi) DirectDB() *redis.Client {
	return r.kv
}

func (r *Redi) GetKeyOnGame(gameID string, key string) *redis.StringCmd {
	return r.kv.HGet(context.Background(), fmt.Sprintf("game:%s", gameID), key)
}

func (r *Redi) SetKeyOnGame(gameID string, key string, value interface{}) *redis.IntCmd {
	return r.kv.HSet(context.Background(), fmt.Sprintf("game:%s", gameID), key, value)
}

func (r *Redi) GetKeyOnPlayer(playerID string, key string) *redis.StringCmd {
	return r.kv.HGet(context.Background(), fmt.Sprintf("players:%s", playerID), key)
}

func (r *Redi) SetKeyOnPlayer(playerID string, key string, value interface{}) *redis.IntCmd {
	return r.kv.HSet(context.Background(), fmt.Sprintf("players:%s", playerID), key, value)
}

func (r *Redi) GetGamePlayerIDs(gameID string) ([]string, error) {
	return r.kv.LRange(context.Background(), fmt.Sprintf("%s#players", gameID), 0, -1).Result()
}

func (r *Redi) WritePlayerData(playerID string, gameID string, channelID string) error {
	// and to redis
	err := r.kv.HSet(context.Background(), fmt.Sprintf("players:%s", playerID), map[string]interface{}{
		"channel_id":   channelID,
		"is_joined":    false,
		"eligible":     true,
		"investigated": false,
	}).Err()
	if err != nil {
		return err
	}

	err = r.kv.RPush(context.Background(), fmt.Sprintf("%s#players", gameID), playerID).Err()
	if err != nil {
		return err
	}

	// also store k=player.ID;v=gameID for lookups
	err = r.kv.HSet(context.Background(), "players>game", map[string]interface{}{
		playerID: gameID,
	}).Err()
	if err != nil {
		return err
	}

	return nil
}

func (r *Redi) GetGameIDUsingPlayer(playerID string) (*string, error) {
	val := r.kv.HGet(context.Background(), "players>game", playerID)
	if err := val.Err(); err != nil {
		return nil, err
	}

	s := val.Val()

	return &s, nil
}

func (r *Redi) GetGlobalChannel(gameID string) (*string, error) {
	val := r.kv.HGet(context.Background(), fmt.Sprintf("game:%s", gameID), "global_channel")
	if err := val.Err(); err != nil {
		return nil, err
	}

	s := val.Val()

	return &s, nil
}

func (r *Redi) GetPlayerChannel(playerID string) (*string, error) {
	val := r.kv.HGet(context.Background(), fmt.Sprintf("players:%s", playerID), "channel_id")
	if err := val.Err(); err != nil {
		return nil, err
	}

	s := val.Val()

	return &s, nil
}

func (r *Redi) GetPlayerIDByNum(gameID string, num int64) (*string, error) {
	val := r.kv.LIndex(context.Background(), fmt.Sprintf("%s#players", gameID), num)
	if err := val.Err(); err != nil {
		return nil, err
	}

	s := val.Val()

	return &s, nil
}

func (r *Redi) GetGamePlayerCount(gameID string) (*uint64, error) {
	val := r.kv.LLen(context.Background(), fmt.Sprintf("%s#players", gameID))
	if err := val.Err(); err != nil {
		return nil, err
	}

	s, err := val.Uint64()
	if err != nil {
		return nil, err
	}

	return &s, nil
}

// interface stuff we won't use
func (r *Redi) Plugin_Help() map[string]string                                    { return map[string]string{} }
func (r *Redi) OnReaction(_ *discordgo.Session, _ interface{}) error              { return nil }
func (r *Redi) OnGuildJoin(_ *discordgo.Session, _ *discordgo.GuildCreate) error  { return nil }
func (r *Redi) OnGuildLeave(_ *discordgo.Session, _ *discordgo.GuildDelete) error { return nil }
func (r *Redi) OnBotClose() error                                                 { return nil }
