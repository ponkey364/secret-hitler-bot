package utils

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"secrethitlerbot/enums"
	"secrethitlerbot/redi"
	"secrethitlerbot/types"
)

func GetTopFromDeck(kv *redi.Redi, gameID string) (*types.Deck, error) {
	// first, get the deck
	deck, err := kv.GetKeyOnGame(gameID, "deck").Uint64()
	if err != nil {
		return nil, err
	}

	// 1 = fascist, 0 = liberal
	var card types.Deck
	if (deck & 1) == 1 {
		card = enums.DECK_FASCIST
	} else {
		card = enums.DECK_LIBERAL
	}

	// sliiiiide to the right
	deck = deck >> 1

	err = kv.DirectDB().HSet(context.Background(), fmt.Sprintf("game:%s", gameID), "deck", deck).Err()
	if err != nil {
		return nil, err
	}

	err = kv.DirectDB().HIncrBy(context.Background(), fmt.Sprintf("game:%s", gameID), "deck_rem", -1).Err()
	if err != nil {
		return nil, err
	}

	return &card, nil
}

func GetKilledPlayers(kv *redi.Redi, gameID string) ([]string, error) {
	players := []string{}
	killed1Num, err := kv.GetKeyOnGame(gameID, "killed_1").Int64()
	if err != nil && err != redis.Nil {
		return nil, err
	}

	if err == nil {
		killed1ID, err := kv.GetPlayerIDByNum(gameID, killed1Num)
		if err != nil {
			return nil, err
		}

		players = append(players, *killed1ID)
	}

	killed2Num, err := kv.GetKeyOnGame(gameID, "killed_2").Int64()
	if err != nil && err != redis.Nil {
		return nil, err
	}

	if err == nil {

		killed2ID, err := kv.GetPlayerIDByNum(gameID, killed2Num)
		if err != nil {
			return nil, err
		}

		players = append(players, *killed2ID)
	}

	return players, nil
}

func SliceIndex(slice []string, want string) int {
	for i, v := range slice {
		if v == want {
			return i
		}
	}
	return -1
}

func IsSenderPresident(gameID string, sender string, r *redi.Redi) (bool, error) {
	presNum, err := r.GetKeyOnGame(gameID, "pres_ptr").Int64()
	if err != nil {
		return false, err
	}

	president, err := r.GetPlayerIDByNum(gameID, presNum)
	if err != nil {
		return false, err
	}

	if *president == sender {
		return true, nil
	}

	return false, nil
}

func IsSenderChancellor(gameID string, sender string, r *redi.Redi) (bool, error) {
	chancellorNum, err := r.GetKeyOnGame(gameID, "chan_ptr").Int64()
	if err != nil {
		return false, err
	}

	chancellor, err := r.GetPlayerIDByNum(gameID, chancellorNum)
	if err != nil {
		return false, err
	}

	if *chancellor == sender {
		return true, nil
	}

	return false, nil
}
